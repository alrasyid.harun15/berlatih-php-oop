<?php 

include_once('animal.php');

/**
 * 
 */
class Frog extends Animal
{
	private string $jump = "Hop ...Hop ....";

	public function jump():string{
		return $this->jump;		
	}

	public function get_legs():int{
		return 4;
	}

	public function get_cold_blooded():bool{
		return true;
	}
}

 ?>