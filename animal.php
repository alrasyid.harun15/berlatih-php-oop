<?php 

/**
 * 
 */
class Animal
{
	
	private int $legs = 2;
	private string $name = '';
	private bool $cold_blooded = false;


	function __construct(string $name){
		$this->name = $name;
	}

	public function get_legs():int{
		return $this->legs;
	}

	public function get_name():string{
		return $this->name;	
	}

	public function get_cold_blooded():bool{
		return $this->cold_blooded;
	}

}




?>