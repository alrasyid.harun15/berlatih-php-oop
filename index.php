<?php 

include_once('animal.php');
include_once('ape.php');
include_once('frog.php');

echo '<h2> Release 0 </h2>';

$sheep = new Animal('Shaun');

echo "Animal class : <br>";
echo $sheep->get_name();
echo "<br>";
echo $sheep->get_legs();
echo "<br>";
var_dump($sheep->get_cold_blooded()); 

echo '<h2> Release 1 </h2>';

$ape = new Ape('Caesar');

echo "Ape class : <br> name : ";
echo $ape->get_name();
echo "<br> num of legs : ";
echo $ape->get_legs();
echo "<br> blood : ";
var_dump($ape->get_cold_blooded()); 
echo "<br> Ape can yell : ";
var_dump($ape->yell());
echo "<br><br>";

$frog = new Frog('Kermit');

echo "Frog class : <br> name : ";
echo $frog->get_name();
echo "<br> num of legs : ";
echo $frog->get_legs();
echo "<br> blood : ";
var_dump($frog->get_cold_blooded());
echo "<br> Frog can Jump : ";
var_dump($frog->jump()); 




?>